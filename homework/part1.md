# Software Testing and TDD - Part 1

## User Story

Compute the net price of all the items in a customer’s shopping cart including all applicable discounts and taxes.

## Functional Requirements

1. If the shopping cart contains 10 or more items, the customers shall get a 10% discount off the purchase price before taxes are included. If the shopping cart contains more than 5 items, the customer shall receive a 5% discount before tax. Otherwise, no discount is applied.

2. Customers may not purchase more than 50 items in a single checkout session.

3. If the customer is a member of the store’s discount shopping club, they shall receive an additional 10% discount off the purchase price before taxes.

4. Unless a customer has tax-exempt status, the local sales tax rate of 4.5% shall be applied to the net discounted price.

5. Dollar amounts shall be rounded to the nearest cent (\$0.01) before and after the tax rate is applied.

6. The net purchase before any discount, the net discount, the tax amount, and the net total due shall all be reported in US dollars.

## Tasks

1.  Analyze the first four functional requirements above (i.e., FR 1-4) and determine the number of test cases needed to provide 100% path and conditional coverage. Use graph theory (i.e., control flow analysis) and / or conditional truth tables to analyze the requirements.
2.  Design additional tests using equivalence partitions and boundary value analysis (BVA) to provide further verification for FR 1 and FR 2.
3.  Design tests for FR 5 using EP and BVA. Pay attention to the float-point nature of the values!
4.  Create a document enumerating each test case and describe the intention of the test and the expected outcome.

## Pseudocode

Pseudocode:

```
procedure COMPUTE_NETPRICE (Items-items in the cart, Customer)
    count=count_of(Items)
    isMemberOfDiscountClub=Customer->isMemberOfDisCountClub
    isTaxExempt=Customer->Customer->isTaxExempt

    if count>50 then
        exit code:1 message: Cannot checkout more than 50 items at once

    total_price=calculate_total_price(Items)
    total_discount
    net_price_before_tax
    tax_amount
    net_price

    if count>=10 then
        total_discount=total_price*10%
    elseif count>5 then
        total_discount=total_price*5%
    else total_discount=0

    net_price_before_tax=total_price-total_discount

    if isMemberOfDiscountClub==TRUE then
        total_discount=total_discount+net_price_before_tax*10%

    total_discount=ROUNT_CENTS(total_discount)
    net_price_before_tax=total_price-total_discount

    if isTaxExempt==FALSE then
        tax_amount=ROUND_CENTS(net_price_before_tax*4.5%)

    net_price=net_price_before_tax+tax_amount

    exit code:0 return:total_price, total_discount, tax_amount, net_price

procedure ROUND_CENTS

```

## Control Flow Graph

![Control Flog Graph](./../cfg.png)

## Task 1 - Conditional coverage

There are 5 conditional branching in my algorithm and I shall define them as below

-   CON1 - count>50
-   CON2 - count>=10
-   CON3 - count>5
-   CON4 - isMemberOfDiscountClub==TRUE
-   CON5 - isTaxExempt==TRUE

Then conditional truth table looks like below.

| Test # | CON 1 | CON 2 | CON 3 | CON 4 | CON 5 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| 1      | T     | T     | T     | T     | T     |
| 2      | T     | T     | T     | T     | F     |
| 3      | T     | T     | T     | F     | T     |
| 4      | T     | T     | T     | F     | F     |
| 5      | T     | T     | F     | T     | T     |
| 6      | T     | T     | F     | T     | F     |
| 7      | T     | T     | F     | F     | T     |
| 8      | T     | T     | F     | F     | F     |
| 9      | T     | F     | T     | T     | T     |
| 10     | T     | F     | T     | T     | F     |
| 11     | T     | F     | T     | F     | T     |
| 12     | T     | F     | T     | F     | F     |
| 13     | T     | F     | F     | T     | T     |
| 14     | T     | F     | F     | T     | F     |
| 15     | T     | F     | F     | F     | T     |
| 16     | T     | F     | F     | F     | F     |
| 17     | F     | T     | T     | T     | T     |
| 18     | F     | T     | T     | T     | F     |
| 19     | F     | T     | T     | F     | T     |
| 20     | F     | T     | T     | F     | F     |
| 21     | F     | T     | F     | T     | T     |
| 22     | F     | T     | F     | T     | F     |
| 23     | F     | T     | F     | F     | T     |
| 24     | F     | T     | F     | F     | F     |
| 25     | F     | F     | T     | T     | T     |
| 26     | F     | F     | T     | T     | F     |
| 27     | F     | F     | T     | F     | T     |
| 28     | F     | F     | T     | F     | F     |
| 29     | F     | F     | F     | T     | T     |
| 30     | F     | F     | F     | T     | F     |
| 31     | F     | F     | F     | F     | T     |
| 32     | F     | F     | F     | F     | F     |

Since once CON1 is TRUE, other conditional branchings do not work, Test #1-16 can be deducted to single case.

| Test # | CON 1 | CON 2 | CON 3 | CON 4 | CON 5 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| 1      | T     | -     | -     | -     | -     |
| 17     | F     | T     | T     | T     | T     |
| 18     | F     | T     | T     | T     | F     |
| 19     | F     | T     | T     | F     | T     |
| 20     | F     | T     | T     | F     | F     |
| 21     | F     | T     | F     | T     | T     |
| 22     | F     | T     | F     | T     | F     |
| 23     | F     | T     | F     | F     | T     |
| 24     | F     | T     | F     | F     | F     |
| 25     | F     | F     | T     | T     | T     |
| 26     | F     | F     | T     | T     | F     |
| 27     | F     | F     | T     | F     | T     |
| 28     | F     | F     | T     | F     | F     |
| 29     | F     | F     | F     | T     | T     |
| 30     | F     | F     | F     | T     | F     |
| 31     | F     | F     | F     | F     | T     |
| 32     | F     | F     | F     | F     | F     |

Since once CON2 is TRUE, CON3 does not run, TEST #21-24 can be eliminated.

| Test # | CON 1 | CON 2 | CON 3 | CON 4 | CON 5 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| 1      | T     | -     | -     | -     | -     |
| 17     | F     | T     | -     | T     | T     |
| 18     | F     | T     | -     | T     | F     |
| 19     | F     | T     | -     | F     | T     |
| 20     | F     | T     | -     | F     | F     |
| 25     | F     | F     | T     | T     | T     |
| 26     | F     | F     | T     | T     | F     |
| 27     | F     | F     | T     | F     | T     |
| 28     | F     | F     | T     | F     | F     |
| 29     | F     | F     | F     | T     | T     |
| 30     | F     | F     | F     | T     | F     |
| 31     | F     | F     | F     | F     | T     |
| 32     | F     | F     | F     | F     | F     |

Therefore, at least 13 test cases listed above should be implemented to cover all conditions.

## Task 1 - Path coverage

As seen from the CFG, there should be 6 test cases at least to qualify path coverage.
The conditions for basis paths to be implemented.

| Test # | CON 1 | CON 2 | CON 3 | CON 4 | CON 5 |
| ------ | ----- | ----- | ----- | ----- | ----- |
| 1      | T     | -     | -     | -     | -     |
| 2      | F     | T     | -     | T     | T     |
| 3      | F     | F     | T     | T     | T     |
| 4      | F     | F     | F     | T     | T     |
| 5      | F     | F     | T     | F     | T     |
| 6      | F     | F     | T     | T     | F     |

Note: I could not understand how to find the independent basis paths. The [article here](http://www.onestoptesting.com/code-coverage/path-coverage.asp) helped me a lot to find them.

## Task 2

**count** could fall in these partitions: [0->5],[6->9],[10->50],[51->]<br/>
Therefore, the these values below should be tested:[0,1,4,5],[6,7,8,9],[10,11,49,50],[51,52]<br/>
And if I combine these numbers with the conditional coverage table, below test cases

| Test # | counts | isMemberOfDisCountClub | isTaxExempt |
| ------ | ------ | ---------------------- | ----------- |
| 1      | 52     | T                      | T           |
| 2      | 51     | T                      | F           |
| 3      | 50     | T                      | T           |
| 4      | 49     | T                      | F           |
| 5      | 11     | F                      | T           |
| 6      | 10     | F                      | F           |
| 7      | 9      | T                      | T           |
| 8      | 8      | T                      | F           |
| 9      | 7      | F                      | T           |
| 10     | 6      | F                      | F           |
| 11     | 5      | T                      | T           |
| 12     | 4      | T                      | F           |
| 13     | 1      | F                      | T           |
| 14     | 4      | F                      | F           |
| 15     | 0      | T                      | T           |

## Task 3

To qualify FR5, ROUND_CENTS procedure should be implemented.
I think EPs of this problem are:

1. negative numbers - [-Float.MAX_VALUE as a BV, -1-as a NV ; -0.001-as a BV, -Float.MIN_VALUE]
2. non negative natural numbers, - [0-as a BV; 1-as a NV, Float.MAX_VALUE as a BV]
3. non negative decimal numbers whose thousandths digist belongs to [0,5) - [Float.MIN_VALUE, 0.0001, 4.342, 1.0049, 2.9949]
4. non negative decimal numbers whose thousandths digit belongs to [5,9] - [0.005, 5.346, 1.4599, 3.9999]

Test cases for ROUND_CENTS: 

| Test case        | Result          |
| ---------------- | --------------- |
| -Float.MAX_VALUE | error           |
| -1               | error           |
| -0.001           | error           |
| -Float.MIN_VALUE | error           |
| 0                | 0               |
| 1                | 1               |
| Float.MAX_VALUE  | Float.MAX_VALUE |
| Float.MIN_VALUE  | 0.0             |
| 0.0001           | 0.0             |
| 4.342            | 4.34            |
| 1.0049           | 1.004           |
| 2.9949           | 2.99            |
| 0.005            | 0.01            |
| 5.346            | 5.35            |
| 1.4599           | 1.46            |
| 3.9999           | 4.0             |