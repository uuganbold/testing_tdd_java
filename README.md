# Loyola COMP474 - Software Engineering

In this project, I implemented a functionality of shopping cart with TDD.

## Codes

The implementation of the functionality and its tests can be found from below.

1. [DataBaseShoppingCart.java](./src/main/java/edu/luc/comp474/DataBaseShoppingCart.java) - java code implementing required functionality
2. [DataBaseShoppingCartTest.java](./src/test/java/edu/luc/comp474/DataBaseShoppingCartTest.java) - testing cases to test the implementation.

## Testing Frameworks

1. [JUnit 5](https://junit.org/junit5/) - Unit testing framework
2. [Mockito](https://site.mockito.org/) - Mocking framework for unit tests.
3. [JaCoCo](https://www.eclemma.org/jacoco/) - Java code coverage library.
4. [Checkstyle](https://checkstyle.sourceforge.io/) - Coding standard checker.

## Test Log

You can see test result of unit testing from the file [test.log](./homework/test.log).
This file's commit history includes results of each iteration of the implementation.

## Test Coverage

Test cases qualifies 100% statement and branch coverage.

![test code coverage](homework/testcoverage.png)

## Run tests

It is a [Maven](https://maven.apache.org/) project and in order to run the tests you need to [install](https://maven.apache.org/download.cgi) maven on your machine. (unless your IDE supports JUnit testing)

After you installed it, I could run the test with code below.

```shell
mvn test
```

## Bug Fixes + Improvements

1. Fixed incorrect usage of _assertEquals_. I used expected values where actual values should be and vice versa.
2. Added checking negative parameter in _roundCents_ method. It was forgotten and caught with the test.
3. Fixed incorrect usage of _assertEquals_. I had passed integer numbers as expected values when I checked float numbers.
4. Fixed incorrect initialization of mock object.
5. Made _roundCents_ implementation used BigDecimal because of floating point number's weird behavours.
6. Fixed rounding bug by adding two more calss of _roundCents_.
7. Fixed bugs on test code. Bugs were because of incorrect usage of _assertEquals_
8. Added a test checking the case on which passed item was not found from the database. It was identified by test coverage report.
9. Added test coded for POJO classes which was left not implemented. It was identified by test coverage report.
