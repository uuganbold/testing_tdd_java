package edu.luc.comp474;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ItemTest {

    @Test
    public void givenConstructorWithValue_whenCustruct_thenValuesBeSet() {
        Item item = new Item("qwert", 233f);
        assertEquals("qwert", item.getName());
        assertEquals(233f, item.getPrice());
    }

    @Test
    public void givenSetterWithValue_whenSetValue_thenValuesBeSet() {
        Item item = new Item("qwert", 34.5f);
        assertEquals("qwert", item.getName());
        assertEquals(34.5f, item.getPrice());

        item.setName("newName");
        assertEquals("newName", item.getName());

        item.setPrice(345.3f);
        assertEquals(345.3f, item.getPrice());
    }

    @Test
    public void givenItem_whenToString_thenReturnString() {
        Item item = new Item("product_name", 345.5f);
        assertEquals("{ name='product_name' , price='345.5' }", item.toString());
    }
}