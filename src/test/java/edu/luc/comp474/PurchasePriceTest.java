package edu.luc.comp474;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PurchasePriceTest {

    @Test
    public void givenDefaultConstructor_whenCustruct_thenValuesShouldZero() {
        PurchasePrice price = new PurchasePrice();
        assertEquals(0, price.getTotalPrice());
        assertEquals(0, price.getTotalDiscount());
        assertEquals(0, price.getNetPurchaseBeforeTax());
        assertEquals(0, price.getTaxAmount());
        assertEquals(0, price.getNetPurchase());
    }

    @Test
    public void givenConstructorWithValue_whenCustruct_thenValuesBeSet() {
        PurchasePrice price = new PurchasePrice(1.1f, 2.4f, 3.5f, 4.5f, 5.9f);
        assertEquals(1.1f, price.getTotalPrice());
        assertEquals(2.4f, price.getTotalDiscount());
        assertEquals(3.5f, price.getNetPurchaseBeforeTax());
        assertEquals(4.5f, price.getTaxAmount());
        assertEquals(5.9f, price.getNetPurchase());
    }

    @Test
    public void givenSetterWithValue_whenSetValue_thenValuesBeSet() {
        PurchasePrice price = new PurchasePrice();

        price.setTotalPrice(5.6f);
        price.setTotalDiscount(2f);
        price.setNetPurchaseBeforeTax(3.4f);
        price.setTaxAmount(8.4f);
        price.setNetPurchase(9f);

        assertEquals(5.6f, price.getTotalPrice());
        assertEquals(2f, price.getTotalDiscount());
        assertEquals(3.4f, price.getNetPurchaseBeforeTax());
        assertEquals(8.4f, price.getTaxAmount());
        assertEquals(9f, price.getNetPurchase());
    }

    @Test
    public void givenPurchasePrice_whenToString_thenReturnString() {
        PurchasePrice price = new PurchasePrice(1.1f, 2.4f, 3.5f, 4.5f, 5.9f);
        assertEquals("{ totalPrice='1.1' , totalDiscount='2.4' , netPurchaseBeforeTax='3.5' , taxAmount='4.5"
                + "' , netPurchase='5.9' }", price.toString());

    }
}