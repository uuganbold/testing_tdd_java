package edu.luc.comp474;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class CustomerTest {

    @Test
    public void givenDefaultConstructor_whenCustruct_thenValuesShouldFalse() {
        Customer customer = new Customer();
        assertFalse(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());
    }

    @Test
    public void givenConstructorWithValue_whenCustruct_thenValuesBeSet() {
        Customer customer = new Customer(true, false);
        assertTrue(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());

        customer = new Customer(false, true);
        assertFalse(customer.isMemberOfDiscountClub());
        assertTrue(customer.isTaxExempt());

        customer = new Customer(true, true);
        assertTrue(customer.isMemberOfDiscountClub());
        assertTrue(customer.isTaxExempt());

        customer = new Customer(false, false);
        assertFalse(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());
    }

    @Test
    public void givenSetterWithValue_whenSetValue_thenValuesBeSet() {
        Customer customer = new Customer();
        assertFalse(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());

        customer.setMemberOfDiscountClub(true);
        assertTrue(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());

        customer.setTaxExempt(true);
        assertTrue(customer.isMemberOfDiscountClub());
        assertTrue(customer.isTaxExempt());

        customer.setMemberOfDiscountClub(false);
        assertFalse(customer.isMemberOfDiscountClub());
        assertTrue(customer.isTaxExempt());

        customer.setTaxExempt(false);
        assertFalse(customer.isMemberOfDiscountClub());
        assertFalse(customer.isTaxExempt());
    }

    @Test
    public void givenCustomer_whenToString_thenReturnString() {
        Customer customer = new Customer();
        assertEquals("{ isMemberOfDiscountClub='false' , isTaxExempt='false' }", customer.toString());

        customer = new Customer(true, true);
        assertEquals("{ isMemberOfDiscountClub='true' , isTaxExempt='true' }", customer.toString());

    }
}