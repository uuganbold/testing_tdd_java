package edu.luc.comp474;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Test cases for DataBaseShoppingCart.
 */

@ExtendWith(MockitoExtension.class)
public class DataBaseShoppingCartTest {

    @Mock
    private DataBase dataBase;

    private DataBaseShoppingCart shoppingCart;

    /**
     * This method created random string with 10 length. It is used for generate
     * item ids randomly.
     *
     * @return Random string that is generated.
     */
    private String getRandomString() {
        final int leftLimit = 97; // letter 'a'
        final int rightLimit = 122; // letter 'z'
        final int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();

        return generatedString;
    }

    @BeforeEach
    public void prepareTest() {
        shoppingCart = new DataBaseShoppingCart(dataBase);
    }

    /**
     * Tests if shopping cart accepts more than 50 items.
     */
    @Test
    public void shouldThrowExceptionWhenTooManyProductsInCart() {
        // given
        final int size = 52;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 1);
        }

        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            // testing with 52 items
            shoppingCart.calculatePurchasePrice(itemIds, new Customer());
        });

        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            // testing with 51 items
            shoppingCart.calculatePurchasePrice(itemIds.subList(0, size - 1), new Customer());
        });

    }

    @Test
    public void shouldNotCountIfItemIsNotFoundFromDatabase() {
        // given
        when(dataBase.getItem(any(String.class))).thenReturn(null);
        List<String> itemIds = new ArrayList<>();
        itemIds.add(getRandomString());
        itemIds.add(getRandomString());
        itemIds.add(getRandomString());

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, true));

        // then
        assertEquals(0f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(0f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(0f, purchase.getNetPurchaseBeforeTax(), "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(0f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 50, discount club: T, tax exempt:T
     */
    public void shouldAccept50ItemsDiscountClubTaxExempt() {
        // given
        final int size = 50;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            // all prices are 0
            items[i] = new Item(itemIds.get(i), 0f);
        }
        // only price of first item=1237.25f, and it makes total price:1237.25f
        items[0].setPrice(1237.25f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, true));

        // then
        /*
         * totalPrice=1237.25 discount=1237.25*10%=123.725,
         * net_value=1237.25-123.725=1113.525 discountClub=1113.525*10%=111.3525
         * net_value=1113.525-111.3525=1002.1725
         * total_discount=123.725+111.3525=235.0775~~235.08
         * net_value_before_tax=1237.25-235.08=1002.17
         * 
         * tax=0; net_value=1002.17
         */
        assertEquals(1237.25f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(235.08f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(1002.17f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(1002.17f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 49, discount club: T, tax exempt:F
     */
    public void shouldAccept49ItemsDiscountClubNoTaxExempt() {
        // given
        final int size = 49;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            // 0.00, 1.01, 2.02, 3.03... 48.48
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(1187.76f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, false));

        // then
        /*
         * totalPrice=1187.76 discount=1187.76*10%=118.776,
         * net_value=1187.76-118.776=1068.984 discountClub=1068.984*10%=106.8984
         * net_value=1068.984-106.8984=962.0856
         * total_discount=118.776+106.8984=225.6744~~225.67
         * net_value_before_tax=1187.76-225.67=962.09
         * 
         * tax=962.09*4.5%= 43.29405~~43.29 net_value=962.09+43.29=1005.38
         */
        assertEquals(1187.76f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(225.67f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(962.09f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(43.29f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(1005.38f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 11, discount club: F, tax exempt:T
     */
    public void shouldAccept11ItemsNoDiscountClubTaxExempt() {
        // given
        final int size = 11;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(55.55f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, true));

        // then
        /*
         * sum(0.00,1.01,2.02...10.10)=55.55 discount=55.55*10%=5.555,
         * net_value=55.55-5.555=49.995 discountClub=False 0% total_discount=5.555~~5.56
         * net_value_before_tax=55.55-5.56=49.99
         * 
         * tax=0 net_value=49.99
         */
        assertEquals(55.55f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(5.56f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(49.99f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(49.99f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 10, discount club: F, tax exempt:F
     */
    public void shouldAccept10ItemsNoDiscountClubNoTaxExempt() {
        // given
        final int size = 10;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(24.34f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, false));

        // then
        /*
         * totalPrice=24.34f discount=24.34*10%=2.434, net_value=24.34-2.434=21.906
         * discountClub=False 0% total_discount=2.434~~2.43
         * net_value_before_tax=24.34-2.43=21.91 tax=4.5%=21.91*4.5%=0.98595~~~0.99
         * net_value=21.91+0.99=22.9
         */
        assertEquals(24.34f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(2.43f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(21.91f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0.99f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(22.9f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 9, discount club: T, tax exempt:T
     */
    public void shouldAccept9ItemsDiscountClubTaxExempt() {
        // given
        final int size = 9;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(24.34f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, true));

        // then
        /*
         * totalPrice=24.34f discount=24.34*5%=1.217, net_value=24.34-1.217=23.123
         * discountClub=True 10%=23.123*10%=2.3123
         * total_discount=2.3123+1.217=3.5293~~3.53
         * net_value_before_tax=24.34-3.53=20.81 tax=0%=0 net_value=20.81+0=20.81
         */
        assertEquals(24.34f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(3.53f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(20.81f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(20.81f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 8, discount club: T, tax exempt:F
     */
    public void shouldAccept8ItemsDiscountClubNoTaxExempt() {
        // given
        final int size = 8;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(24.34f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, false));

        // then
        /*
         * totalPrice=24.34f discount=24.34*5%=1.217, net_value=24.34-1.217=23.123
         * discountClub=True 10%=23.123*10%=2.3123
         * total_discount=2.3123+1.217=3.5293~~3.53
         * net_value_before_tax=24.34-3.53=20.81 tax=4.5%=20.81*4.5%=0.93645~~~0.94
         * net_value=20.81+0.94=21.75
         */
        assertEquals(24.34f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(3.53f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(20.81f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0.94f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(21.75f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 7, discount club: F, tax exempt:T
     */
    public void shouldAccept7ItemsNoDiscountClubTaxExempt() {
        // given
        final int size = 7;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(24.34f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, true));

        // then
        /*
         * totalPrice=24.34f discount=24.34*5%=1.217, net_value=24.34-1.217=23.123
         * discountClub=False 0%=0 total_discount=1.217~~1.22
         * net_value_before_tax=24.34-1.22=23.12 tax=0%=0 net_value=23.12+0=23.12
         */
        assertEquals(24.34f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(1.22f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(23.12f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(23.12f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 6, discount club: F, tax exempt:F
     */
    public void shouldAccept6ItemsNoDiscountClubNoTaxExempt() {
        // given
        final int size = 6;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(24.34f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, false));

        // then
        /*
         * totalPrice=24.34f discount=24.34*5%=1.217, net_value=24.34-1.217=23.123
         * discountClub=False 0%=0 total_discount=1.217~~1.22
         * net_value_before_tax=24.34-1.22=23.12 tax=4.5%=23.12*4.5%=1.0404~~~1.04
         * net_value=23.12+1.04=24.16
         */
        assertEquals(24.34f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(1.22f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(23.12f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(1.04f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(24.16f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 5, discount club: T, tax exempt:T
     */
    public void shouldAccept5ItemsDiscountClubTaxExempt() {
        // given
        final int size = 5;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(100.0f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, true));

        // then
        /*
         * totalPrice=100f discount=0%=0, net_value=100 discountClub=True 10%=10
         * total_discount=10 net_value_before_tax=100-10=90 tax=0%=0 net_value=90+0=90
         */
        assertEquals(100f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(10f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(90f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(90f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 4, discount club: T, tax exempt:F
     */
    public void shouldAccept5ItemsDiscountClubNoTaxExempt() {
        // given
        final int size = 4;
        Item[] items = new Item[size];
        List<String> itemIds = new ArrayList<>(size);
        for (int i = 0; i < items.length; i++) {
            itemIds.add(getRandomString());
            items[i] = new Item(itemIds.get(i), 0f);
        }
        items[0].setPrice(150.0f);
        when(dataBase.getItem(any(String.class))).thenReturn(items[0], Arrays.copyOfRange(items, 1, size - 1));

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, false));

        // then
        /*
         * totalPrice=150f discount=0%=0, net_value=150 discountClub=True 10%=15
         * total_discount=15 net_value_before_tax=150-15=135
         * tax=4.5%=135*4.5%=6.075~~6.08 net_value=135+6.08=141.08
         */
        assertEquals(150f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(15f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(135f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(6.08f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(141.08f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 1, discount club: F, tax exempt:T
     */
    public void shouldAccept1ItemsNoDiscountClubTaxExempt() {
        // given
        String itemId = getRandomString();
        float value = 25f;
        when(dataBase.getItem(anyString())).thenReturn(new Item(getRandomString(), value));
        List<String> itemIds = new ArrayList<>();
        itemIds.add(itemId);

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(false, true));

        // then
        /*
         * totalPrice=25f discount=0%=0, net_value=25 discountClub=false 0%=0
         * total_discount=0 net_value_before_tax=25f tax=0%=0 net_value=25
         */
        assertEquals(25f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(0f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(25f, purchase.getNetPurchaseBeforeTax(),
                "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(25f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * counts 0, discount club: T, tax exempt:T
     */
    public void shouldAccept0ItemsDiscountClubTaxExempt() {
        // given
        List<String> itemIds = new ArrayList<>();

        // when
        PurchasePrice purchase = shoppingCart.calculatePurchasePrice(itemIds, new Customer(true, true));

        // then
        /*
         * totalPrice=25f discount=0%=0, net_value=25 discountClub=false 0%=0
         * total_discount=0 net_value_before_tax=25f tax=0%=0 net_value=25
         */
        assertEquals(0f, purchase.getTotalPrice(), "Total amount does not match with expected value");
        assertEquals(0f, purchase.getTotalDiscount(), "Total discount does not match with expected value");
        assertEquals(0f, purchase.getNetPurchaseBeforeTax(), "Net value before tax does not match with expected value");
        assertEquals(0f, purchase.getTaxAmount(), "Tax value does not match with expected value");
        assertEquals(0f, purchase.getNetPurchase(), "Net value does not match with expected value");
    }

    @Test
    /**
     * items:null, discount club: T, tax exempt:T
     */
    public void shouldNotAcceptNullItemsOrCustomer() {
        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            // testing with null items
            shoppingCart.calculatePurchasePrice(null, new Customer());
        });

        // then
        assertThrows(IllegalArgumentException.class, () -> {
            // when
            // testing with null customer
            shoppingCart.calculatePurchasePrice(new ArrayList<>(), null);
        });
    }

    @Test
    /**
     * Tests if roundCents accepts negative dollar.
     */
    public void shouldMakeErrorWhenRoundNegativeDollars() {
        assertThrows(IllegalArgumentException.class, () -> {
            shoppingCart.roundCents(-Float.MAX_VALUE);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            shoppingCart.roundCents(-1);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            final float negFloat = -0.001f;
            shoppingCart.roundCents(negFloat);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            shoppingCart.roundCents(-Float.MIN_VALUE);
        });
    }

    @Test
    /**
     * roundCents should not make any change to integer value.
     */
    public void shouldBeSameWhenRoundIntegerValue() {
        assertEquals(0f, shoppingCart.roundCents(0));
        assertEquals(1f, shoppingCart.roundCents(1));
        assertEquals(Float.MAX_VALUE, shoppingCart.roundCents(Float.MAX_VALUE));
    }

    @Test
    /**
     * roundCents should round down if thousandths digit is [0-4]
     */
    public void shouldBeRoundedDownWhenThousandthsLessThan5() {
        assertEquals(0f, shoppingCart.roundCents(Float.MIN_VALUE));
        assertEquals(0f, shoppingCart.roundCents(0.0001f));
        assertEquals(4.34f, shoppingCart.roundCents(4.342f));
        assertEquals(1f, shoppingCart.roundCents(1.0049f));
        assertEquals(2.99f, shoppingCart.roundCents(2.9949f));
    }

    @Test
    /**
     * roundCents should round up if thousandths digit is [5-9]
     */
    public void shouldBeRoundedUpWhenThousandthsEqualsOrGreaterThan5() {
        assertEquals(0.01f, shoppingCart.roundCents(0.005f));
        assertEquals(5.35f, shoppingCart.roundCents(5.346f));
        assertEquals(1.46f, shoppingCart.roundCents(1.4599f));
        assertEquals(4f, shoppingCart.roundCents(3.9999f));
    }

}
