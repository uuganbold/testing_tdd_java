package edu.luc.comp474;

/**
 * Entity object to hold Customer information.
 *
 * @author Uuganbold Tsegmed
 */
public class Customer {

    /**
     * Whether the customer is member of Discount club or not.
     */
    private boolean memberOfDiscountClub;

    /**
     * Whether the customer has tax-exempt status.
     */
    private boolean taxExempt;

    public Customer() {
    }

    public Customer(boolean memberOfDiscountClub, boolean taxExempt) {
        this.memberOfDiscountClub = memberOfDiscountClub;
        this.taxExempt = taxExempt;
    }

    public boolean isMemberOfDiscountClub() {
        return this.memberOfDiscountClub;
    }

    public void setMemberOfDiscountClub(boolean memberOfDiscountClub) {
        this.memberOfDiscountClub = memberOfDiscountClub;
    }

    public boolean isTaxExempt() {
        return this.taxExempt;
    }

    public void setTaxExempt(boolean taxExempt) {
        this.taxExempt = taxExempt;
    }

    @Override
    public String toString() {
        return "{" + " isMemberOfDiscountClub='" + memberOfDiscountClub + "' , isTaxExempt='" + taxExempt + "' }";
    }

}
