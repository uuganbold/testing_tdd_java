package edu.luc.comp474;

/**
 * Entity object to hold item information.
 *
 * @author Uuganbold Tsegmed
 */
public class Item {

    /**
     * Product ID (name).
     */
    private String name;

    /**
     * Product price.
     */
    private float price;

    public Item(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        return "{" + " name='" + getName() + "' , price='" + getPrice() + "' }";
    }

}
