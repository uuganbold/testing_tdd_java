package edu.luc.comp474;

/**
 * Entity object that holds information about purchase.
 */
public class PurchasePrice {

    /**
     * Total purchase price before any deduction or addition.
     */
    private float totalPrice;

    /**
     * Total discount to be deducted from the purchase.
     */
    private float totalDiscount;

    /**
     * Net Purchase price before tax added.
     */
    private float netPurchaseBeforeTax;

    /**
     * Tax amount to be added.
     */
    private float taxAmount;

    /**
     * Net purchase amount after all deduction and addition calculated.
     */
    private float netPurchase;

    /**
     * Creates PurchasePrice object with default values.
     */
    public PurchasePrice() {
    }

    /**
     * Created PurchasePrice object with the values passed by arguments.
     */
    public PurchasePrice(float totalPrice, float totalDiscount, float netPurchaseBeforeTax, float taxAmount,
            float netPurchase) {
        this.totalPrice = totalPrice;
        this.totalDiscount = totalDiscount;
        this.netPurchaseBeforeTax = netPurchaseBeforeTax;
        this.taxAmount = taxAmount;
        this.netPurchase = netPurchase;
    }

    public float getTotalPrice() {
        return this.totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public float getTotalDiscount() {
        return this.totalDiscount;
    }

    public void setTotalDiscount(float totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public float getNetPurchaseBeforeTax() {
        return this.netPurchaseBeforeTax;
    }

    public void setNetPurchaseBeforeTax(float netPurchaseBeforeTax) {
        this.netPurchaseBeforeTax = netPurchaseBeforeTax;
    }

    public float getTaxAmount() {
        return this.taxAmount;
    }

    public void setTaxAmount(float taxAmount) {
        this.taxAmount = taxAmount;
    }

    public float getNetPurchase() {
        return this.netPurchase;
    }

    public void setNetPurchase(float netPurchase) {
        this.netPurchase = netPurchase;
    }

    @Override
    public String toString() {
        return "{" + " totalPrice='" + getTotalPrice() + "' , totalDiscount='" + getTotalDiscount()
                + "' , netPurchaseBeforeTax='" + getNetPurchaseBeforeTax() + "' , taxAmount='" + getTaxAmount()
                + "' , netPurchase='" + getNetPurchase() + "' }";
    }
}
