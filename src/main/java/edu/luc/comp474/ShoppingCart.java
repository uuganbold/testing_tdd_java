package edu.luc.comp474;

import java.util.List;

/**
 * Interface providing basic shopping cart operation.
 *
 * @author Uuganbold Tsegmed
 * @version 1.0
 */
public interface ShoppingCart {

    /**
     * This interface should calculate total price, discounts, tax amounts and net
     * prices before tax and after tax.
     *
     * @param productIDs ID's of the products in the cart
     * @param customer   Customer being making purchase
     * @return @see PurchasePrice object which hold purchase information.
     */
    PurchasePrice calculatePurchasePrice(List<String> productIDs, Customer customer);
}
