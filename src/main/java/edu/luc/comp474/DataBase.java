package edu.luc.comp474;

/**
 * Interface providing basic database operation.
 *
 * @author Uuganbold Tsegmed
 */
public interface DataBase {

    /**
     * Retrieves single product from the database.
     *
     * @param name Product ID
     * @return Product item with the id passed by argument.
     */
    Item getItem(String name);
}
