package edu.luc.comp474;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @see edu.luc.comp474.ShoppingCart implementation using DataBase. This class
 *      should read product information from the database.
 */
public class DataBaseShoppingCart implements ShoppingCart {

    /**
     * DataBase from which product information is read.
     */
    private final DataBase dataBase;

    /**
     * Creates ShoppingCart with corresponding database.
     *
     * @param dataBase
     */
    public DataBaseShoppingCart(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    /**
     * Implementation of @see ShoppingCart#calculatePurchasePrice(List, Customer) It
     * reads product prices from database and calculated purchase prices, discounts,
     * tax information.
     *
     * @param productIDs products ids in the cart
     * @param customer   Customer making purchase
     * @return @see PurchasePrice value calculated with the method
     */
    public PurchasePrice calculatePurchasePrice(List<String> productIDs, Customer customer) {
        final int maxAllowedItems = 50;
        final int discount10PercentNumber = 10;
        final int discount5PercentNumber = 5;
        final float tenPercent = 0.1f;
        final float fivePercent = 0.05f;
        final float fourDotFivePercent = 0.045f;

        if (productIDs == null) {
            throw new IllegalArgumentException("Product ID's must be passed");
        }
        if (customer == null) {
            throw new IllegalArgumentException("Customer must be passed");
        }

        List<Item> products = productIDs.stream().map(id -> dataBase.getItem(id)).filter(i -> i != null)
                .collect(Collectors.toList());
        if (products.size() > maxAllowedItems) {
            throw new IllegalArgumentException("Cart cannot have more than 50 items");
        }

        PurchasePrice purchasePrice = new PurchasePrice(0, 0, 0, 0, 0);

        float totalPurchase = 0;
        float totalDiscount = 0;
        float netPurchaseBeforeTax = 0;
        float taxAmount = 0;
        float netPurchase;

        for (Item item : products) {
            totalPurchase += item.getPrice();
        }

        if (products.size() >= discount10PercentNumber) {
            totalDiscount += totalPurchase * tenPercent;
        } else if (products.size() > discount5PercentNumber) {
            totalDiscount += totalPurchase * fivePercent;
        }
        netPurchaseBeforeTax = totalPurchase - totalDiscount;

        if (customer.isMemberOfDiscountClub()) {
            totalDiscount += netPurchaseBeforeTax * tenPercent;
        }
        totalDiscount = roundCents(totalDiscount);
        netPurchaseBeforeTax = roundCents(totalPurchase - totalDiscount);

        if (!customer.isTaxExempt()) {
            taxAmount = roundCents(netPurchaseBeforeTax * fourDotFivePercent);
        }

        netPurchase = roundCents(netPurchaseBeforeTax + taxAmount);

        purchasePrice.setTotalPrice(totalPurchase);
        purchasePrice.setTotalDiscount(totalDiscount);
        purchasePrice.setNetPurchaseBeforeTax(netPurchaseBeforeTax);
        purchasePrice.setTaxAmount(taxAmount);
        purchasePrice.setNetPurchase(netPurchase);

        return purchasePrice;
    }

    /**
     * This method is used for US dollar amount to round nearest cent value.
     *
     * @param amount US dollar value
     * @return
     */
    public float roundCents(float amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("The value to be rounded should not be negative.");
        }
        BigDecimal value = new BigDecimal(Float.toString(amount));
        value = value.setScale(2, RoundingMode.HALF_UP);
        return value.floatValue();
    }

}
